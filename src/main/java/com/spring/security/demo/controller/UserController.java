package com.spring.security.demo.controller;

import com.spring.security.demo.entity.model.PageParam;
import com.spring.security.demo.entity.vo.userVO.*;
import com.spring.security.demo.service.UserService;
import com.spring.security.demo.utils.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.*;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@RestController
@Api(tags = "用户模块")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("user")
public class UserController {

    private final UserDetailsService userDetailsService;

    private final UserService userService;

    private final ConsumerTokenServices consumerTokenServices;

    @GetMapping("/admin/hello")
    public String admin() {
        return "hello admin";
    }

    @GetMapping("/user/hello")
    public String user() {
        return "hello user";
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    @PostMapping("/logOut")
    public boolean logOut(@RequestBody TokenVO tokenQo) {

        return consumerTokenServices.revokeToken(tokenQo.getAccessToken());
    }

    @PostMapping("/getAcl")
    public void getAcl() {

         userService.getAuthentication();
    }

    @ApiOperation(value = "注册用户")
    @PostMapping("/register")
    public RequestResult register(@RequestBody RegisterVO registerVO) {
        return userService.register(registerVO);
    }

    @ApiOperation(value = "获取登陆的用户信息")
    @PostMapping("/getUser")
    public RequestResult getUserByToken(@RequestBody TokenVO tokenVO){
         return userService.getUserByToken(tokenVO.getAccessToken());
    }

    @ApiOperation(value = "通过用户id获取用户信息")
    @PostMapping("/getUserById")
    public RequestResult getUserById(@RequestBody UserIdVO userIdVO){
        return userService.getUserById(userIdVO);
    }

    @ApiOperation(value = "分页获取用户")
    @PostMapping("/getAllUser")
    public RequestResult getAllUser(@RequestBody PageParam pageParam){
        return userService.getAllUser(pageParam);
    }

    @ApiOperation(value = "更新用户的信息")
    @PostMapping("/updateUser")
    public RequestResult updateUser(@RequestBody UpdateUserVO updateUserVO){
        return userService.updateUser(updateUserVO);
    }

    @ApiOperation(value = "删除用户" ,notes = "以禁用的形式加删除")
    @PostMapping("/disableUser")
    public RequestResult disableUser(@RequestBody UserIdVO userIdVO){
        return userService.disableUser(userIdVO);
    }

    @PostMapping("add")
    public String add(){
        return "add";
    }
    @PostMapping("delete")
    public String delete(){
        return "delete";
    }
    @PostMapping("update")
    public String update(){
        return "update";
    }
    @PostMapping("query")
    public String query(){
        return "query";
    }

    @GetMapping("getTest")
    public String query(@RequestParam("name") String name){
        return name;
    }

}
