package com.spring.security.demo.controller;

import com.spring.security.demo.entity.vo.roleVO.*;
import com.spring.security.demo.service.RoleService;
import com.spring.security.demo.utils.RequestResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author :Raiz
 * @date :2020/7/27
 */

@RestController
@Api(tags = "角色模块")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("role")
public class RoleController {

    private final RoleService roleService;

    @ApiOperation(value = "添加角色")
    @PostMapping("/addRole")
    RequestResult addRole(@RequestBody RoleNameVO roleNameDTO){
        return roleService.addRole(roleNameDTO);
    }

    @ApiOperation(value = "删除角色")
    @PostMapping("/deleteRole")
    RequestResult deleteRole(@RequestBody RoleIdVO idDto){
        return roleService.deleteRole(idDto);
    }

    @ApiOperation(value = "更新角色")
    @PostMapping("/updateRole")
    RequestResult addRole(@RequestBody RoleVO roleDTO){
        return roleService.updateRole(roleDTO);
    }

    @ApiOperation(value = "查询角色的权限")
    @PostMapping("/queryRoleMenu")
    RequestResult queryRoleMenu(@RequestBody RoleIdVO idDTO){
        return roleService.queryRoleMenu(idDTO);
    }

    @ApiOperation(value = "获取所有的角色")
    @PostMapping("/getAllRole")
    RequestResult queryRoleMenu(){
        return roleService.getAllRole();
    }

    @ApiOperation(value = "设置用户的角色")
    @PostMapping("/setUserRole")
    RequestResult setUserRole(@RequestBody SetUserRoleVO userRoleVO){
        return roleService.setUserRole(userRoleVO);
    }

    @ApiOperation(value = "设置角色的权限")
    @PostMapping("/setRoleMenu")
    RequestResult setRoleMenu(@RequestBody SetRoleMenuVO setRoleMenuVO){
        return roleService.setRoleMenu(setRoleMenuVO);
    }
}
