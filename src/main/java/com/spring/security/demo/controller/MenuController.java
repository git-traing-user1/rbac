package com.spring.security.demo.controller;

import com.spring.security.demo.entity.vo.menuModuleVO.MenuModuleVO;
import com.spring.security.demo.service.MenuModuleService;
import com.spring.security.demo.utils.RequestResult;
import com.spring.security.demo.entity.vo.menuVO.AddMenuVO;
import com.spring.security.demo.entity.vo.menuVO.MenuIdVO;
import com.spring.security.demo.entity.vo.menuVO.UpdateMenuVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@RestController
@Api(tags = "权限模块")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@RequestMapping("menuModule")
public class MenuController {

    private final MenuModuleService menuModuleService;

    @ApiOperation(value = "添加权限模块")
    @PostMapping("/addMenuModule")
    public RequestResult addMenuModule(@RequestBody MenuModuleVO menuModuleDto){
        return menuModuleService.addMenuModel(menuModuleDto);
    }

    @ApiOperation(value = "查询权限树")
    @PostMapping("/queryMenuModel")
    public RequestResult queryMenuModel(){
        return menuModuleService.queryMenuModel();
    }

    @ApiOperation(value = "添加权限菜单")
    @PostMapping("/addMenu")
    public RequestResult addMenu(AddMenuVO addMenuVO){
        return menuModuleService.addMenu(addMenuVO);
    }

    @ApiOperation(value = "删除权限菜单")
    @PostMapping("/deleteMenu")
    public RequestResult deleteMenu(MenuIdVO menuIdVO){
        return menuModuleService.deleteMenu(menuIdVO);
    }

    @ApiOperation(value = "更新权限菜单")
    @PostMapping("/updateMenu")
    public RequestResult updateMenu(UpdateMenuVO updateMenuVO){
        return menuModuleService.updateMenu(updateMenuVO);
    }

    @ApiOperation(value = "查询所有权限")
    @PostMapping("/queryAllMenu")
    public RequestResult queryAllMenu(){
        return menuModuleService.queryAllMenu();
    }
}
