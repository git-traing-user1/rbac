package com.spring.security.demo.utils;


import com.spring.security.demo.constant.ResponseConstant;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

public class RequestResult {

    private String code;

    private String msg;

    private Object data;

    public RequestResult() {}

    public RequestResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public static RequestResult success() {
        RequestResult Result = new RequestResult();
        Result.setMsg(ResponseConstant.OPERATION_SUCCESS);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        return Result;
    }

    public static RequestResult success(String msg) {
        RequestResult Result = new RequestResult();
        Result.setMsg(msg);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        return Result;
    }


    public static RequestResult success(Object data) {
        RequestResult Result = new RequestResult();
        Result.setMsg(ResponseConstant.OPERATION_SUCCESS);
        Result.setCode(ResponseConstant.SUCCESS_CODE);
        Result.setData(data);
        return Result;
    }

    public static RequestResult failure(String msg) {
        RequestResult Result = new RequestResult();
        Result.setCode(ResponseConstant.ERROR_CODE);
        Result.setMsg(msg);
        return Result;
    }

    public static RequestResult failure(String code, String msg, Object data) {
        RequestResult Result = new RequestResult();
        Result.setCode(code);
        Result.setMsg(msg);
        Result.setData(data);
        return Result;
    }

    public String getCode() {
        return this.code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(final String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return this.data;
    }

    public void setData(final Object data) {
        this.data = data;
    }
}
