package com.spring.security.demo.config;

import com.spring.security.demo.filter.AuthFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

/**
 *资源服务器
 *
 * @author :Raiz
 * @date :2020/7/21
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(ResourceServerSecurityConfigurer resources) {
        OAuthParam oAuthParam = new OAuthParam();
        resources.resourceId(oAuthParam.getClientId()) // 配置资源id，这里的资源id和授权服务器中的资源id一致
                .stateless(true); // 设置这些资源仅基于令牌认证
    }



    // 配置 URL 访问权限
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/user/register").permitAll()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/swagger-ui.html","/v2/**","/webjars/**","/swagger-resources/**").permitAll()
                .anyRequest().authenticated();
        http.addFilter(new AuthFilter());
    }
}
