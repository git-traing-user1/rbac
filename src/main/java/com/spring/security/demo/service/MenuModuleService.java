package com.spring.security.demo.service;

import com.spring.security.demo.entity.vo.menuModuleVO.MenuModuleVO;
import com.spring.security.demo.utils.RequestResult;
import com.spring.security.demo.entity.vo.menuVO.AddMenuVO;
import com.spring.security.demo.entity.vo.menuVO.MenuIdVO;
import com.spring.security.demo.entity.vo.menuVO.UpdateMenuVO;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
public interface MenuModuleService {

    /**
     * 添加权限模块
     *
     * @param menuModuleDto 权限模块信息
     * @return 返回操作结果
     */
    RequestResult addMenuModel(MenuModuleVO menuModuleDto);

    /**
     * 删除权限模块
     *
     * @return 返回操作结果
     */
    RequestResult deleteMenuModel();

    /**
     * 修改权限模块
     *
     * @return 返回操作结果
     */
    RequestResult updateMenuModel();

    /**
     * 返回所有的权限模块和它对应的权限信息
     *
     * @return 返回权限树
     */
    RequestResult queryMenuModel();

    /**
     * 添加权限
     *
     * @param addMenuVO 添加的权限信息
     * @return 返回操作结果
     */
    RequestResult addMenu(AddMenuVO addMenuVO);

    /**
     * 删除权限
     *
     * @param menuIdVO 权限的id
     * @return 返回操作结果
     */
    RequestResult deleteMenu(MenuIdVO menuIdVO);

    /**
     * 更新权限信息
     *
     * @param updateMenuVO 所需要更新的权限信息
     * @return 返回操作结果
     */
    RequestResult updateMenu(UpdateMenuVO updateMenuVO);

    /**
     * 返回所有的权限信息
     *
     * @return 返回权限信息
     */
    RequestResult queryAllMenu();
}
