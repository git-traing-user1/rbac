package com.spring.security.demo.service;

import com.spring.security.demo.entity.vo.roleVO.*;
import com.spring.security.demo.utils.RequestResult;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
public interface RoleService {

    /**
     * 添加角色
     *
     * @param roleNameVO 需要添加的角色信息
     * @return 返回操作结果
     */
    RequestResult addRole(RoleNameVO roleNameVO);

    /**
     * 删除角色
     *
     * @param idDTO 角色id
     * @return 返回操作结果
     */
    RequestResult deleteRole(RoleIdVO idDTO);

    /**
     * 修改角色
     *
     * @param roleDTO 角色需要更新的相关信息
     * @return 返回操作结果
     */
    RequestResult updateRole(RoleVO roleDTO);

    /**
     * 查询角色的权限
     *
     * @param idDTO 角色id
     * @return 返回角色的权限
     */
    RequestResult queryRoleMenu(RoleIdVO idDTO);

    /**
     * 获取所有的角色
     *
     * @return 返回所有的角色
     */
    RequestResult getAllRole();

    /**
     * 设置用户角色
     *
     * @param setUserRoleVO 用户id和角色的id
     * @return 返回操作结果
     */
    RequestResult setUserRole(SetUserRoleVO setUserRoleVO);

    /**
     * 设置角色的权限
     *
     * @param setRoleMenuVO 角色id和权限的id
     * @return 返回操作结果
     */
    RequestResult setRoleMenu(SetRoleMenuVO setRoleMenuVO);

}
