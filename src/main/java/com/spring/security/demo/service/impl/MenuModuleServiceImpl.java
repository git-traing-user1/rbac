package com.spring.security.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.spring.security.demo.constant.MenuConstant;
import com.spring.security.demo.dao.MenuDao;
import com.spring.security.demo.dao.MenuModuleDao;
import com.spring.security.demo.entity.vo.menuModuleVO.MenuModuleVO;
import com.spring.security.demo.entity.model.Menu;
import com.spring.security.demo.entity.model.MenuModule;
import com.spring.security.demo.entity.dto.menuModuleDTO.MenuTreeDTO;
import com.spring.security.demo.service.MenuModuleService;
import com.spring.security.demo.utils.RequestResult;
import com.spring.security.demo.entity.vo.menuVO.AddMenuVO;
import com.spring.security.demo.entity.vo.menuVO.MenuIdVO;
import com.spring.security.demo.entity.vo.menuVO.UpdateMenuVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author :Raiz
 * @date :2020/7/27
 */

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class MenuModuleServiceImpl implements MenuModuleService {

    private final MenuModuleDao menuModuleDao;

    private final MenuDao menuDao;

    private final BeanCopier moduleCopier = BeanCopier.create(MenuModule.class, MenuTreeDTO.class, false);

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult addMenuModel(MenuModuleVO menuModuleDto) {
        MenuModule menuModule = new MenuModule();
        BeanCopier beanCopier = BeanCopier.create(MenuModuleVO.class, MenuModule.class, false);
        beanCopier.copy(menuModuleDto, menuModule, null);

        int result = menuModuleDao.insert(menuModule);
        if (result > 0) {
            return RequestResult.success();
        } else {
            return RequestResult.failure(MenuConstant.ADD_MENU_MODULE_FIAL);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult deleteMenuModel() {
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult updateMenuModel() {
        return null;
    }

    @Override
    public RequestResult queryMenuModel() {
        // 查询权限模块和权限
        List<MenuModule> menuModuleList = menuModuleDao.selectList(new LambdaQueryWrapper<MenuModule>().orderByAsc(MenuModule::getLevel)
                .orderByAsc(MenuModule::getSeq));
        List<Menu> menuList = menuDao.selectList(null);

        List<MenuTreeDTO> menuTreeList = new ArrayList<>();

        // 遍历权限模块生产权限树
        for (MenuModule module : menuModuleList) {
            int level = module.getLevel();
            buildMenuTree(level, menuList, module, menuTreeList);
        }
        return RequestResult.success(menuTreeList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult addMenu(AddMenuVO addMenuVO) {
        Menu menu = new Menu();
        BeanCopier beanCopier = BeanCopier.create(AddMenuVO.class, Menu.class, false);
        beanCopier.copy(addMenuVO, menu, null);
        if (judgeMenu(menu)) {
            RequestResult.failure(MenuConstant.MENU_REPEAT);
        }

        int result = menuDao.insert(menu);
        if (result > 0) {
            return RequestResult.success();
        }

        return RequestResult.failure(MenuConstant.ADD_MENU_FIAL);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult deleteMenu(MenuIdVO menuIdVO) {
        Long id = menuIdVO.getId();

        int result = menuDao.deleteById(id);
        if (result > 0) {
            return RequestResult.success();
        }

        return RequestResult.failure(MenuConstant.MENU_DONT_EXIST);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult updateMenu(UpdateMenuVO updateMenuVO) {
        Menu menu = new Menu();
        BeanCopier beanCopier = BeanCopier.create(UpdateMenuVO.class, Menu.class, false);
        beanCopier.copy(updateMenuVO, menu, null);
        if (judgeMenu(menu)) {
            RequestResult.failure(MenuConstant.MENU_REPEAT);
        }

        int result = menuDao.updateById(menu);
        if (result > 0) {
            return RequestResult.success();
        }

        return RequestResult.failure(MenuConstant.UPDATE_MENU_FIAL);
    }

    @Override
    public RequestResult queryAllMenu() {

        List<Menu> menus = menuDao.selectList(null);

        return RequestResult.success(menus);
    }

    private void buildMenuTree(int level, List<Menu> menuList, MenuModule module, List<MenuTreeDTO> menuTreeList) {
        // 最底层建立权限模块
        if (level == 0) {
            MenuTreeDTO node = new MenuTreeDTO();
            moduleCopier.copy(module, node, null);

            // 取相应模块的权限
            Long moduleId = module.getId();
            List<Menu> menus = menuList.stream().filter(menu -> menu.getModuleId().equals(moduleId)).collect(Collectors.toList());

            // 加入相应的权限
            if (!menus.isEmpty()) {
                node.setMenuList(menus);
            }

            //加入权限树节点
            menuTreeList.add(node);

            //build下一层权限模块
            buildMenuTree(level + 1, menuList, module, menuTreeList);

        } else {
            Long parentId = module.getParentId();
            for (MenuTreeDTO node : menuTreeList) {
                if (node.getId().equals(parentId)) {
                    MenuTreeDTO children = new MenuTreeDTO();
                    moduleCopier.copy(module, children, null);

                    // 取相应模块的权限
                    List<Menu> menus = menuList.stream().filter(menu -> menu.getModuleId().equals(module.getId())).collect(Collectors.toList());
                    if (!menus.isEmpty()) {
                        children.setMenuList(menus);
                    }

                    node.getMenuTree().add(children);
                    //build下一层权限模块
                    buildMenuTree(level + 1, menuList, module, node.getMenuTree());
                    break;
                } else if (!node.getMenuTree().isEmpty()) {
                    //build下一层权限模块
                    buildMenuTree(level + 1, menuList, module, node.getMenuTree());
                }
            }
        }

    }

    private boolean judgeMenu(Menu menu) {
        int result = menuDao.selectCount(new LambdaQueryWrapper<Menu>().eq(Menu::getMenuName, menu.getMenuName())
                .or().eq(Menu::getMenuParam, menu.getMenuParam())
                .or().eq(Menu::getMenuUrl, menu.getMenuUrl()));
        return result <= 0;
    }
}
