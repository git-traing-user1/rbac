package com.spring.security.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.spring.security.demo.dao.MenuDao;
import com.spring.security.demo.dao.MenuModuleDao;
import com.spring.security.demo.dao.RoleDao;
import com.spring.security.demo.dao.UserDao;
import com.spring.security.demo.entity.model.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserDao userDao;

    private final RoleDao roleDao;

    private final MenuDao menuDao;

    private final MenuModuleDao menuModuleDao;

    @Override
    public UserEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        // 根据用户名查找用户
        User user = userDao.selectOne(new LambdaQueryWrapper<User>().eq(User::getUsername, username).eq(User::getDisable, 0));
        if (user != null) {
            Long userId = user.getId();

            //根据用户id获取用户角色
            List<Role> roles = roleDao.getRoleByUserId(userId);
            List<String> roleList = new ArrayList<>();

            // 填充权限
            Collection<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
            for (Role role : roles) {
                roleList.add(role.getRoleName());
            }
            //填充权限菜单
            List<Menu> menus;
            Set<Long> menuModuleId = new HashSet<>();
            Boolean checkSuperUser = userDao.checkSuperUser(userId) > 0;
            if (checkSuperUser) {
                menus = menuDao.selectList(null);
            } else {
                menus = menuDao.getRoleMenuByRoles(roles);
            }

            List<String> menuStringList = new ArrayList<>();
            for (Menu menu : menus) {
                authorities.add(new SimpleGrantedAuthority(menu.getMenuParam()));
                menuModuleId.add(menu.getModuleId());
            }
            // 填充路由菜单
            List<MenuModule> menuModuleList = new ArrayList<>();
            if (checkSuperUser) {
                menuModuleList = menuModuleDao.selectList(null);
            } else {
                if (menuModuleId.size() > 0){
                    menuModuleList = menuModuleDao.selectBatchIds(menuModuleId);
                }
            }
            for (MenuModule menuModule : menuModuleList) {
                menuStringList.add(menuModule.getPath());
            }

            UserEntity userEntity = new UserEntity(user.getId(), username, user.getPassword(), roleList, menuStringList, authorities);
            return userEntity;
        } else {

            throw new UsernameNotFoundException(username + " not found");
        }

    }


}

