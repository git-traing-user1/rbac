package com.spring.security.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.spring.security.demo.constant.RoleConstant;
import com.spring.security.demo.constant.UserConstant;
import com.spring.security.demo.dao.MenuDao;
import com.spring.security.demo.dao.RoleDao;
import com.spring.security.demo.dao.UserDao;
import com.spring.security.demo.entity.model.User;
import com.spring.security.demo.entity.vo.roleVO.*;
import com.spring.security.demo.entity.model.Menu;
import com.spring.security.demo.entity.model.Role;
import com.spring.security.demo.service.RoleService;
import com.spring.security.demo.utils.RequestResult;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;

    private final MenuDao menuDao;

    private final UserDao userDao;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult addRole(RoleNameVO roleNameDto) {
        String roleName = roleNameDto.getRoleName();

        // 判断角色名是否已经存在
        if (judgeRoleName(roleName)) {
            Role newRole = new Role(roleName);

            // 插入数据库用户信息
            int insertResult = roleDao.insert(newRole);
            if (insertResult > 0) {
                return RequestResult.success();
            }
        }
        return RequestResult.failure(RoleConstant.ADD_ROLE_FIAL);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult deleteRole(RoleIdVO idDTO) {
        Long id = idDTO.getId();

        if (roleDao.countUserRole(id) > 0 ){
            return RequestResult.failure(RoleConstant.ROLE_MENU_EXIST );
        }

        // 删除指定id角色
        int result = roleDao.deleteById(id);
        if (result > 0) {
            return RequestResult.success();
        } else {
            return RequestResult.failure(RoleConstant.DELETE_ROLE_FIAL);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult updateRole(RoleVO roleVO) {
        Role role = new Role();
        BeanCopier beanCopier = BeanCopier.create(RoleVO.class, Role.class, false);
        beanCopier.copy(roleVO, role, null);

        if (judgeRoleName(roleVO.getRoleName())) {
            // 更新角色
            int result = roleDao.updateById(role);

            if (result > 0) {
                return RequestResult.success();
            }
        }

        return RequestResult.failure(RoleConstant.DELETE_ROLE_FIAL);
    }

    @Override
    public RequestResult queryRoleMenu(RoleIdVO idDTO) {
        // 获取角色权限
        Long id = idDTO.getId();
        List<Menu> menuList = menuDao.getMenuByRoleId(id);

        return RequestResult.success(menuList);
    }

    @Override
    public RequestResult getAllRole() {
        List<Role> roles = roleDao.selectList(null);

        return RequestResult.success(roles);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult setUserRole(SetUserRoleVO setUserRoleVO) {
        Long userId = setUserRoleVO.getUserId();
        if (!judgeUserExist(userId)) {
            return RequestResult.failure(UserConstant.USER_NOT_EXIST);
        }
        // 删除原有用户的角色
        roleDao.deleteUserRole(userId);
        // 赋予用户新的角色
        roleDao.addUserRole(userId, setUserRoleVO.getRoleIdList());

        return RequestResult.success();
    }

    @Override
    public RequestResult setRoleMenu(SetRoleMenuVO setRoleMenuVO) {
        Long roleId = setRoleMenuVO.getRoleId();
        if (!judgeRoleExist(roleId)){
            return RequestResult.failure(RoleConstant.ROLE_DONT_EXIST);
        }
        // 删除角色原有的权限
        roleDao.deleteRoleMenu(roleId);
        // 赋予角色新的权限
        roleDao.addRoleMenu(roleId, setRoleMenuVO.getMenuIdList());

        return RequestResult.success();
    }

    private boolean judgeUserExist(Long userId) {
        return userDao.selectCount(new LambdaQueryWrapper<User>().eq(User::getId, userId)) > 0;
    }

    private boolean judgeRoleName(String roleName){
        return roleDao.selectCount(new LambdaQueryWrapper<Role>().eq(Role::getRoleName, roleName)) == 0;
    }
    private boolean judgeRoleExist(Long roleId){
        return roleDao.selectCount(new LambdaQueryWrapper<Role>().eq(Role::getId, roleId)) > 0;
    }
}
