package com.spring.security.demo.service;

import com.spring.security.demo.entity.model.PageParam;
import com.spring.security.demo.entity.vo.userVO.*;
import com.spring.security.demo.utils.RequestResult;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
public interface UserService {

    /**
     * 注册用户
     *
     * @param registerQo 用户id和要删除的商品信息
     * @return 返回操作结果
     */
    RequestResult register(RegisterVO registerQo);

    /**
     * 获取登陆的用户权限
     *
     * @return 返回权限信息
     */
    RequestResult getAuthentication();

    /**
     * 获取登陆的用户信息
     *
     * @param token token信息
     * @return 返回用户信息
     */
    RequestResult getUserByToken(String token);

    /**
     * 根据用户id获取角色
     *
     * @param userIdVO token信息
     * @return 返回用户信息
     */
    RequestResult getUserById(UserIdVO userIdVO);

    /**
     * 获取所有用户
     *
     * @param pageParam 分页信息
     * @return 返回用户信息
     */
    RequestResult getAllUser(PageParam pageParam);



    /**
     * 更新用户相关信息
     *
     * @param updateUserVO 需要更新的用户信息
     * @return 返回操作结果
     */
    RequestResult updateUser(UpdateUserVO updateUserVO);

    /**
     * 禁用用户
     *
     * @param userIdVO 用户id
     * @return 返回操作结果
     */
    RequestResult disableUser(UserIdVO userIdVO);
}
