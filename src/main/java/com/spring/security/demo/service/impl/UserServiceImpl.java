package com.spring.security.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.github.pagehelper.page.PageMethod;
import com.spring.security.demo.constant.UserConstant;
import com.spring.security.demo.dao.MenuDao;
import com.spring.security.demo.dao.RoleDao;
import com.spring.security.demo.dao.UserDao;
import com.spring.security.demo.entity.dto.userDTO.UserDTO;
import com.spring.security.demo.entity.dto.userDTO.UserDetailDTO;
import com.spring.security.demo.entity.model.*;
import com.spring.security.demo.entity.vo.userVO.*;
import com.spring.security.demo.service.UserService;
import com.spring.security.demo.utils.RequestResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.beans.BeanCopier;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceImpl implements UserService {

    @Bean
    public PasswordEncoder passwordEncoder() {
        // 使用BCrypt强哈希函数加密方案（密钥迭代次数默认为10）
        return new BCryptPasswordEncoder();
    }

    private final UserDao userDao;

    private final RoleDao roleDao;

    private final MenuDao menuDao;


    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult register(RegisterVO registerQo) {
        String username = registerQo.getUsername();
        String password = registerQo.getPassword();

        // 判断用户名是否已经存在
        if (judgeUserExist(username)) {
            return RequestResult.failure(UserConstant.USER_EXIST);
        }

        User user = new User(username, passwordEncoder().encode(password));
        int result = userDao.insert(user);
        if (result != 0) {
            return RequestResult.success("注册成功");
        }
        return RequestResult.failure("注册失败");
    }

    @Override
    public RequestResult getAuthentication() {
        UserEntity userEntity = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Collection<? extends GrantedAuthority> authorities = SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        return RequestResult.success(authorities);
    }

    @Override
    public RequestResult getUserByToken(String token) {
        UserEntity user = (UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserVO userVo = new UserVO();
        BeanCopier beanCopier = BeanCopier.create(UserEntity.class, UserVO.class, false);


        Collection<? extends GrantedAuthority> authorities = user.getAuthorities();
        List<String> auth = authorities.stream().map(c -> c.getAuthority()).collect(Collectors.toList());
        userVo.setAuthorities(auth);
        beanCopier.copy(user, userVo, null);

        return RequestResult.success(userVo);
    }

    @Override
    public RequestResult getUserById(UserIdVO userIdVO) {
        Long userId = userIdVO.getId();
        User user = userDao.selectOne(new LambdaQueryWrapper<User>().eq(User::getId,userId));
        if (user  ==  null){
            RequestResult.failure(UserConstant.USER_NOT_EXIST);
        }
        //根据用户id获取用户角色
        List<Role> roles = roleDao.getRoleByUserId(userId);
        List<String> roleList = new ArrayList<>();

        for (Role role : roles) {
            roleList.add(role.getRoleName());
        }
        //填充权限菜单
        List<Menu> menus = menuDao.getRoleMenuByRoles(roles);

        List<String> menuStringList = new ArrayList<>();
        for (Menu menu : menus) {
            menuStringList.add(menu.getMenuParam());
        }

        UserDetailDTO userDetail = new UserDetailDTO(userId,user.getUsername(),roleList,menuStringList);

        return RequestResult.success(userDetail);
    }

    @Override
    public RequestResult getAllUser(PageParam pageParam) {
        Integer pageNum = pageParam.getPageNum();
        Integer pageSize = pageParam.getPageSize();

        PageMethod.startPage(pageNum,pageSize);
        List<UserDTO> userList = userDao.getAllUser();
        PageInfo<UserDTO> pageInfo = new PageInfo<>(userList);

        return RequestResult.success(pageInfo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult updateUser(UpdateUserVO updateUserVO) {
        String username = updateUserVO.getUsername();
        String password = updateUserVO.getPassword();

        // 判断用户名是否已存在
        if (judgeUserExist(username)) {
            return RequestResult.failure(UserConstant.USER_EXIST);
        }
        // 密码加密
        updateUserVO.setPassword(passwordEncoder().encode(password));

        BeanCopier beanCopier = BeanCopier.create(UpdateUserVO.class, User.class, false);
        User user = new User();
        beanCopier.copy(updateUserVO, user, null);

        // 将用户信息存入数据库
        int result = userDao.updateById(user);
        if (result > 0) {
            return RequestResult.success();
        }
        return RequestResult.failure(UserConstant.UPDATE_USER_FIAL);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RequestResult disableUser(UserIdVO userIdVO) {
        Long id = userIdVO.getId();

        // 禁用用户
        int result = userDao.disableUser(id);
        if (result > 0) {
            return RequestResult.success();
        }

        return RequestResult.failure(UserConstant.DELETE_USER_FIAL);
    }

    private boolean judgeUserExist(String username) {
        // 判断用户名是否重复
        return userDao.selectCount(new LambdaQueryWrapper<User>().eq(User::getUsername, username)) > 0;
    }
}
