package com.spring.security.demo.entity.vo.userVO;

import lombok.Data;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/27
 */

@Data
public class UserVO {

    private Long id;// 用户id
    private String username;// 用户名
    private List<String> userRoles;// 用户角色集合
    private List<String> menus; // 权限集合
    private List<String> authorities;
}
