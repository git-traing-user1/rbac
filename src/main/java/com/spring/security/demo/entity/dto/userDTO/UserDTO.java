package com.spring.security.demo.entity.dto.userDTO;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
@Data
public class UserDTO {

    private Long id;// 用户id
    private String username;// 用户名
    private Integer disable;
}
