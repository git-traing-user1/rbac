package com.spring.security.demo.entity.vo.roleVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Data
public class RoleIdVO {
    Long id;
}
