package com.spring.security.demo.entity.dto.menuModuleDTO;

import com.spring.security.demo.entity.model.Menu;
import com.spring.security.demo.entity.model.MenuModule;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class MenuTreeDTO extends MenuModule {


    List<MenuTreeDTO> menuTree;

    List<Menu> menuList;

    public MenuTreeDTO(){
        this.menuTree = new ArrayList<MenuTreeDTO>();
        this.menuList = new ArrayList<Menu>();
    }
}
