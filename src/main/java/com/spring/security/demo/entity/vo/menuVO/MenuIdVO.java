package com.spring.security.demo.entity.vo.menuVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/29
 */

@Data
public class MenuIdVO {
    Long id;
}
