package com.spring.security.demo.entity.vo.userVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/21
 */

@Data
public class LoginVO {

    String userName;
    String password;
}
