package com.spring.security.demo.entity.vo.userVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
@Data
public class UserIdVO {

    Long id;
}
