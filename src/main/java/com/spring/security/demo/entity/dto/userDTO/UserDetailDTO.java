package com.spring.security.demo.entity.dto.userDTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/30
 */
@Data
@AllArgsConstructor
public class UserDetailDTO {

    private Long id;

    private String username;

    private List<String> userRoles;

    private List<String> menus;
}
