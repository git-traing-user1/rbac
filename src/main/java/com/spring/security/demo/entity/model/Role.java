package com.spring.security.demo.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Data
public class Role implements Serializable {

    @TableId(type = IdType.AUTO)
    Long id;

    String roleName;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date createdTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date updateTime;

    public Role(){

    }

    public Role(String roleName){
        this.roleName = roleName;
    }
}
