package com.spring.security.demo.entity.vo.roleVO;

import lombok.Data;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
@Data
public class SetRoleMenuVO {

    Long roleId;

    List<Long> menuIdList;
}
