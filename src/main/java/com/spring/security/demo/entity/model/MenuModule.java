package com.spring.security.demo.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.lang.reflect.Type;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Data
public class MenuModule {

    @TableId(type = IdType.AUTO)
    Long id;

    String name;

    Long parentId;

    Integer level;

    Integer seq;

    String routeName;

    String componentPath;

    String path;
}
