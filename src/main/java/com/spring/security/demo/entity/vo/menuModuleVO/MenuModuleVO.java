package com.spring.security.demo.entity.vo.menuModuleVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Data
@ApiModel(value = "添加权限模型参数")
public class MenuModuleVO {

    @ApiModelProperty(value = "权限模型名称")
    String name;

    @ApiModelProperty(value = "父级权限模型id")
    Long parentId;

    @ApiModelProperty(value = "权限模型层级")
    Long level;

    @ApiModelProperty(value = "同级权限模型顺序")
    Integer seq;

    @ApiModelProperty(value = "权限模块路由名称")
    String routeName;

    @ApiModelProperty(value = "权限模块组件路径")
    String componentPath;

    @ApiModelProperty(value = "权限模块路径")
    String path;
}
