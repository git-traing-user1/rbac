package com.spring.security.demo.entity.model;

import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Data
public class UserEntity implements UserDetails {
    private static final long serialVersionUID = -9005214545793249372L;

    private Long id;// 用户id
    private String username;// 用户名
    private String password;// 密码
    private List<String> userRoles;// 用户角色集合
    private List<String> menus;

    private Collection<? extends GrantedAuthority> authorities;

    public UserEntity(Long id, String username, String password, List<String> userRoles,List<String> menus, Collection<? extends GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userRoles = userRoles;
        this.menus = menus;
        this.authorities = authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
