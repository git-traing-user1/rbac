package com.spring.security.demo.entity.vo.menuVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
@Data
public class UpdateMenuVO{

    Long id;

    String menuName;

    String menuUrl;

    String menuParam;

    Long moduleId;
}
