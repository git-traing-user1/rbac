package com.spring.security.demo.entity.vo.menuVO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
@Data
@ApiModel(value = "添加权限所需参数")
public class AddMenuVO {

    @ApiModelProperty(value = "权限模块路由名称")
    String menuName;

    @ApiModelProperty(value = "权限请求路径")
    String menuUrl;

    @ApiModelProperty(value = "权限码")
    String menuParam;

    @ApiModelProperty(value = "所属权限模块id")
    Long moduleId;

}
