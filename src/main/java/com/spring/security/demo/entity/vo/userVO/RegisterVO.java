package com.spring.security.demo.entity.vo.userVO;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/22
 */
@Data
public class RegisterVO {

    String username;

    String password;
}
