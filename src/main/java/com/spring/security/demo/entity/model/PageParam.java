package com.spring.security.demo.entity.model;

import lombok.Data;

/**
 * @author :Raiz
 * @date :2020/7/29
 */

@Data
public class PageParam {

    Integer pageNum;

    Integer pageSize;
}
