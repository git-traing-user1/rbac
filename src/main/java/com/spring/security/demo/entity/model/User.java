package com.spring.security.demo.entity.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

/**
 * @author :Raiz
 * @date :2020/7/21
 */

@Data

public class User implements Serializable {

    @TableId(type = IdType.AUTO)
    Long id;

    String username;

    String password;

    Integer disable;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date createdTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    Date updateTime;

    public User(){ }

    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public User(Long id, String username, String password, Integer disable, Timestamp createdTime, Timestamp updateTime) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.disable = disable;
        this.createdTime = createdTime;
        this.updateTime = updateTime;
    }
}
