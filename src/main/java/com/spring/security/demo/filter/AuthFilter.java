package com.spring.security.demo.filter;

import com.alibaba.fastjson.JSON;
import com.spring.security.demo.entity.model.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
public class AuthFilter extends FilterSecurityInterceptor {

    private  List<String> ignoreUrl = Arrays.asList("/user/getUser", "/user/register", "/swagger-ui.html", "/v2/**", "/webjars/**", "/swagger-resources/**");


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String url = request.getRequestURI();

        Object object = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (object instanceof UserEntity) {
            UserEntity userEntity = (UserEntity) object;

            boolean judge = false;
            if (userEntity.getUsername().equals("admin")) {
                judge = true;
            } else if (judgeUrl(url)){
                judge = true;
            }

            // 判断用户的权限和请求的URL
            Set<String> menuSet = new HashSet<>(userEntity.getMenus());
            if (menuSet.contains(url)) {
                judge = true;
            }

            if (judge == false) {
                HttpServletResponse response = (HttpServletResponse) servletResponse;
                response.setCharacterEncoding("UTF-8");
                response.setContentType("application/json; charset=utf-8");
                Map<String, String> map = new HashMap<>();
                map.put("CODE", "400");
                map.put("ERROR", "没有权限请求");
                String jsonOutput = JSON.toJSONString(map);
                servletResponse.getWriter().print(jsonOutput);
                return;
            } else {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        } else if (object instanceof String) {
            if (object.equals("anonymousUser") && judgeUrl(url)) {
                filterChain.doFilter(servletRequest, servletResponse);
                return;
            }
        }

    }

    private boolean judgeUrl(String url) {
        AntPathMatcher antPathMatcher = new AntPathMatcher();
        for (String temp : ignoreUrl) {
            if (antPathMatcher.match(temp, url)) {
                return true;
            }
        }
        return false;
    }
}
