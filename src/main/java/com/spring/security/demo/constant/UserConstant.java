package com.spring.security.demo.constant;

/**
 * @author :Raiz
 * @date :2020/7/29
 */
public class UserConstant {

    public static final Integer DISABLE_STATUS = 1;

    public static final Integer ENABLE_STATUS = 0;

    public static final String USER_NOT_EXIST = "用户不存在";

    public static final String USER_EXIST = "此用户名已存在";

    public static final String REGISTER_FIAL = "注册失败";

    public static final String UPDATE_USER_FIAL = "更新用户信息失败";

    public static final String DELETE_USER_FIAL = "删除用户信息失败";
}
