package com.spring.security.demo.constant;

/**
 * @author:Riaz
 * @date:2020/7/9
 *
 **/

public class ResponseConstant {

    public static final String SUCCESS_CODE = "200";

    public static final String ERROR_CODE = "400";

    public static final String SUCCESS = "success";

    public static final String ERROR = "error";

    public static final String LOGIN_SUCCESS = "login success";

    public static final String REPEAT_LOGIN = "repeat login";

    public static final String OPERATION_SUCCESS = "操作成功";

}
