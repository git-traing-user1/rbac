package com.spring.security.demo.constant;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
public class MenuConstant {

    public static final String  ADD_MENU_MODULE_FIAL = "添加权限模块失败";

    public static final String  MENU_REPEAT = "权限名称或权限路径或权限码重复";

    public static final String  ADD_MENU_FIAL = "添加权限失败";

    public static final String  UPDATE_MENU_FIAL = "更新权限失败";

    public static final String  MENU_DONT_EXIST = "不存在此id权限";
}
