package com.spring.security.demo.constant;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
public class RoleConstant {

    public static final String ADD_ROLE_SUCCESS = "添加角色成功";

    public static final String ADD_ROLE_FIAL = "添加角色失败";

    public static final String DELETE_ROLE_FIAL = "不存在此id的角色";

    public static final String ROLE_MENU_EXIST = "角色仍存在权限";

    public static final String USER_ROLE_EXIST = "仍有用户为此角色";

    public static final String ROLE_DONT_EXIST = "此id角色不存在";
}
