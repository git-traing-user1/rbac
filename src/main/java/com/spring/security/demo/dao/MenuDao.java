package com.spring.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.security.demo.entity.model.Menu;
import com.spring.security.demo.entity.model.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Repository
public interface MenuDao  extends BaseMapper<Menu> {

    /**
     * 批量获取角色的权限
     *
     * @param roleList 角色列表
     * @return 对应角色的权限List
     */
    @Select("<script> " +
            "SELECT menu.id,menu_name,menu_url,menu_param,module_id " +
            "FROM menu LEFT JOIN role_menus rm " +
            "ON menu.id = rm.menu_id " +
            "WHERE rm.role_id IN " +
            " <foreach collection='roleList' item = 'item' open='(' separator=',' close=')'>" +
            " #{item.id}" +
            " </foreach>" +
            "Group by menu.id   " +
            "</script>")
    List<Menu> getRoleMenuByRoles(@Param("roleList") List<Role> roleList);

    /**
     * 获取指定角色的权限
     *
     * @param roleId 角色id
     * @return 对应角色的权限
     */
    @Select("SELECT menu.id,menu_name,menu_url,menu_param " +
            "FROM menu LEFT JOIN role_menus rm " +
            "ON menu.id = rm.menu_id " +
            "WHERE rm.role_id = #{roleId}"
            )
    List<Menu> getMenuByRoleId(@Param("roleId") Long roleId);
}
