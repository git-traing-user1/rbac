package com.spring.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.security.demo.entity.dto.userDTO.UserDTO;
import com.spring.security.demo.entity.model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Repository
public interface UserDao extends BaseMapper<User> {


    /**
     * 禁用角色
     *
     * @param id user的id
     * @return 更新的行数
     */
    @Update("UPDATE user " +
            "SET disable = 1 " +
            "WHERE id = #{id} AND disable = 0")
    int disableUser(@Param("id") Long id);

    /**
     * 返回查询的user列表
     *
     * @return 查询的user列表
     */
    @Select("SELECT id,username,disable " +
            "FROM user")
    List<UserDTO> getAllUser();

    /**
     * 根据userId查询user
     *
     * @param userId user的id
     * @return 查询的user
     */
    @Select("SELECT id,username,disable " +
            "FROM user " +
            "WHERE id = #{userId}")
    UserDTO getUserById(@Param("userId")Long userId);

    /**
     * 根据userId查询user
     *
     * @param userId user的id
     * @return 查询的user
     */
    @Select("SELECT count(1) " +
            "FROM user_super " +
            "WHERE user_id = #{userId}")
    int checkSuperUser(@Param("userId")Long userId);
}
