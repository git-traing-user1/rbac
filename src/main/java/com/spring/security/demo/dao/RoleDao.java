package com.spring.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.security.demo.entity.model.Role;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author :Raiz
 * @date :2020/7/21
 */
@Repository
public interface RoleDao extends BaseMapper<Role> {

    /**
     * 获取指定用户的角色
     *
     * @param id 用户Id
     * @return 对应的角色信息
     */
    @Select("SELECT role.id,role_name " +
            "FROM role LEFT JOIN user_roles ur " +
            "ON role.id = ur.role_id " +
            "WHERE ur.user_id = #{id}")
    List<Role> getRoleByUserId(@Param("id") Long id);

    /**
     * 统计角色的用户数量
     *
     * @param roleId 用户Id
     * @return 指定角色的用户数量
     */
    @Select("SELECT COUNT(1) FROM user_roles " +
            "WHERE role_id = #{roleId}")
    int countUserRole(@Param("roleId") Long roleId);


    /**
     * 删除用户的角色
     *
     * @param id 用户Id
     * @return 影响的数据库行数
     */
    @Delete("Delete FROM user_roles " +
            "WHERE user_id = #{id}")
    int deleteUserRole(@Param("id") Long id);

    /**
     * 批量插入用户的角色
     *
     * @param userId 用户Id
     * @param roleIdList 角色idList
     * @return 影响的数据库行数
     */
    @Insert("<script>" +
            "INSERT INTO user_roles(user_id,role_id) VALUES" +
            "<foreach collection='roleIdList' item='item' index='index' separator=',' >" +
            "(#{userId},#{item})" +
            "</foreach>" +
            "</script>")
    int addUserRole(@Param("userId") Long userId, @Param("roleIdList") List<Long> roleIdList);

    /**
     * 删除角色的权限
     *
     * @param id 用户Id
     * @return 影响的数据库行数
     */
    @Delete("Delete FROM role_menus " +
            "WHERE role_id = #{id}")
    int deleteRoleMenu(@Param("id") Long id);

    /**
     * 添加角色的权限
     *
     * @param roleId 角色id
     * @param menuIdList 权限的idList
     * @return 影响的数据库行数
     */
    @Insert("<script>" +
            "INSERT INTO role_menus(role_id,menu_id) VALUES" +
            "<foreach collection='menuIdList' item='item' index='index' separator=',' >" +
            "(#{roleId},#{item})" +
            "</foreach>" +
            "</script>")
    int addRoleMenu(@Param("roleId") Long roleId, @Param("menuIdList") List<Long> menuIdList);
}
