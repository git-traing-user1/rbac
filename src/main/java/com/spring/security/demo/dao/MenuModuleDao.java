package com.spring.security.demo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.spring.security.demo.entity.model.MenuModule;
import org.springframework.stereotype.Repository;

/**
 * @author :Raiz
 * @date :2020/7/27
 */
@Repository
public interface MenuModuleDao extends BaseMapper<MenuModule> {

}
